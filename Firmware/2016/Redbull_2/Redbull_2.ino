//redbull.ino
//arduino 1.0.6
/*
TODO 
add wdt
Rotate in reverse if read failed
Use shielded wire for barcode scanner


2 barcode button
3 can servo CR 100
4 relay
5 sweep servo 20-170
6 gate servo 70-150
7,8 soft serial 8,7 (rx,tx)
9 rgb led
A3 Timer button
A4 Sharpe sensor
A5 timer blink led
*/

#include <Servo.h>
#include <SoftwareSerial.h>
#include <Adafruit_NeoPixel.h>

const int barcodeButton = 2;
const int canMotor = 3;
const int controllerRelay = 4;
const int sweep = 5;
const int gate = 6;
const int tx = 7;
const int rx = 8;
const int rgb = 9;
const int timerButton = A3;
const int sharpSensor = A4;
const int timer_led = A5;

const int lowfreq = 500;
const int highfreq = 250;

int ledState = LOW;             // ledState used to set the LED
unsigned long previousMillis = 0;        // will store last time LED was updated
unsigned long currentMillis;

int canStop = 90;
int canRotate = 91;
int sweepHome = 175;
int sweepAway = 40;
int gateHome = 175;
int gateAway = 70;

int sensorValue = 0;        // value read from the sensor
int i;
int pos=0;
int x = 0;
 
unsigned long previous=0;
unsigned long Timeout = 2000;
unsigned long canDetectStart = 0;
unsigned int canScanTime = 9000;

unsigned int badBarcodeWaitTime = 9000;
unsigned long  ok0Start = 0;
unsigned long  ok1Start = 0;
unsigned long  elaspsedGameTime = 0;
unsigned long  gameTime = 240000;//2,40,000 ms
//unsigned long  gameTime = 24000;//2,40,000 ms
unsigned long elaspsedTimePercent = 0;

char inchar;
char codeRead[13]={};
//small can:90162602  
//small SF: 90162800
//big can : 9002490210939
//red:      90415128
//white:    90415142
//blue:     90415135
//Yellow:   90424427 (Quatar)
//sf egypt: 90424014

char ok='2';//go direct to loop and wait for can drop

char gameReset='0';
char canDetected = '0';
char response[100];

Servo Can;
Servo Sweep;
Servo Gate;

SoftwareSerial barcode(rx,tx); //rx.tx
//Tx connected to another arduino send command for timer led

Adafruit_NeoPixel strip = Adafruit_NeoPixel(9, rgb, NEO_GRB + NEO_KHZ800);

uint32_t red = strip.Color(127, 0, 0);
uint32_t green = strip.Color(0, 127, 0);
uint32_t blue = strip.Color(0, 0, 127);
uint32_t yellow = strip.Color(255, 100, 0);
uint32_t noColor = strip.Color(0, 0, 0);

//Sensor averaging variables
const int numReadings = 3;

int readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
int total = 0;                  // the running total
int average = 0;                // the average
int First = 1;

void setup() {
  Init();
}
//turn on button
    //start motor
    //timer of 7 seconds
    //dispose can 
void loop() {
  delay(100);
  //if timer is up set ok at two
  total= total - readings[readIndex];         
  readings[readIndex] = analogRead(sharpSensor); 
  total= total + readings[readIndex];       
  readIndex = readIndex + 1;                    
  if (readIndex >= numReadings){
    readIndex = 0;              
  }
  average = total / numReadings;         
  Serial.println(average);
  if (average > 400 &&  canDetected != '1'){//Can detected
    //start timer here
    Serial.println(F("Can detected,timer started"));
    Can.attach(canMotor);
    Can.write(canRotate); // start can rotation
    digitalWrite(barcodeButton, LOW);
    delay(100);
    digitalWrite(barcodeButton,HIGH); //press the barcode button
    canDetected = '1';
    canDetectStart = millis(); //timer start
    Serial.print(F("Detect start: "));
    Serial.println(canDetectStart);
  }
  else{
    //Serial.println("no Can found");
    //canDetected = '0'; //might have to remove this
  }
  if(canDetected == '1' && (millis() - canDetectStart > canScanTime)){
      Serial.println(F("scanning time up"));
      Serial.print(F("Curr Time: "));
      Serial.println(millis());
      digitalWrite(barcodeButton, LOW);
      Can.write(canStop);
      Can.detach();
      disposeCan();
      canDetected = '0';
  }
  if (barcode.available() > 0){// ok variable set   
      //scanAndCompare();
// -------------------------
    x=0;
    memset(response, '\0', 100); 
    previous = millis();
    do{
      if(barcode.available() != 0){   
        response[x] = barcode.read();
        // char tmp = barcode.read();
        // response[x] = ~tmp ;
        Serial.print(response[x]);
        x++;
        if (strstr(response, "2602") != NULL ||  //rb
            strstr(response, "2800") != NULL ||  //rb sf same for qatar
            strstr(response, "21093") != NULL ||  //big can
            strstr(response, "5128") != NULL ||  //red
            strstr(response, "5142") != NULL ||  //white
            strstr(response, "5135") != NULL ||  //blue
            strstr(response, "4427") != NULL ||  //yellow Quatar
            strstr(response, "4014") != NULL || //rb sf egypt
            strstr(response, "9016") != NULL || 
            strstr(response, "9041") != NULL || 
            strstr(response, "9042") != NULL || First == 1
            ){
              First = 0;
              Serial.println(F("Can found"));
              if(Can.attached()){
                Can.write(canStop);
              }
              Can.detach();
              Sweep.detach();
              Gate.detach();
              ok = '1';
              ok1Start = millis();
              break;
        }
        //else start ok0startime and ok = '0'
      }
    }while((millis() - previous) < Timeout); 
// -------------------------
  }//if barcode available end
  if(Serial.available()> 0){
    char tmp = Serial.read();
    if(tmp == 'r'){
      ok= '1';
      ok1Start = millis();
      Serial.println(F("Simulated redbull"));
    }
    if(tmp == 'd'){
      disposeCan();
    }
    
  }
  /*
  ok = 0 -> barcode read is bad or not found
  ok = 1 -> barcode read is of redbull
  ok = 2 -> tristate variable looping
  */
  if(ok == '1'){ //barcode ok    
    elaspsedGameTime = millis() - ok1Start;
    elaspsedTimePercent = map(elaspsedGameTime,0,gameTime,0,100);
    Serial.print(elaspsedGameTime/1000);
    Serial.print(F("  "));
    Serial.println(elaspsedTimePercent);
    if(gameReset == '0'){
      digitalWrite(barcodeButton, LOW);
      digitalWrite(controllerRelay, HIGH);//xbox controller on
      resetGame();
      gameReset = '1';
    }
    if (elaspsedGameTime < gameTime){
      if (digitalRead(timerButton) == LOW)//monitoring the button
        {  // switch is pressed - pullup keeps pin high normally
          delay(100);                        // delay to debounce switch
          ok1Start = millis();
          //barcode.print('0');
          digitalWrite(timer_led, LOW);// timer led off
          colorWipeReverse(green, 100); //  fill the ring
        }
      //Serial.println(elaspsedTimePercent);
      if(elaspsedTimePercent < 10){
        paintStrip(green,0);
        //barcode.print('0');
        digitalWrite(timer_led, LOW);// timer led off
      }
      if(elaspsedTimePercent >10 && elaspsedTimePercent < 20){
        //paintStrip(green,1);
      }
      if(elaspsedTimePercent >20 && elaspsedTimePercent < 30){        
        //paintStrip(green,2);
      }
      if(elaspsedTimePercent >30 && elaspsedTimePercent < 40){        
        paintStrip(green,1);
      }
      if(elaspsedTimePercent >40 && elaspsedTimePercent < 50){        
        paintStrip(green,2);
      }
      if(elaspsedTimePercent >50 && elaspsedTimePercent < 60){        
        paintStrip(green,3);
      }
      if(elaspsedTimePercent >60 && elaspsedTimePercent < 70){        
        paintStrip(green,4);
      }
      if(elaspsedTimePercent >70 && elaspsedTimePercent < 80){        
        paintStrip(yellow,5);
        //barcode.print('1');//slow blink
        blinks(lowfreq);
      }
      if(elaspsedTimePercent >80 && elaspsedTimePercent < 90){        
        paintStrip(yellow,6);
        //barcode.print('2'); //fast blink
        blinks(highfreq);
      }
      if(elaspsedTimePercent >90){        
        paintStrip(red,7);
        //barcode.print('3');//solid
        digitalWrite(timer_led, HIGH);
      }
    }
    else{ //times up 
      //barcode.print('0');
      digitalWrite(timer_led, LOW);// timer led off
      digitalWrite(controllerRelay, LOW);
      gameReset = '0';
      ok = '2';
    }
  }

  else if (ok == '2'){ //loop here
    colorWipeReverse(red, 30); // Red fill the ring
    colorWipeReverse(blue, 30); // Fill the ring
     barcode.println('0');
  }

  else if (ok == '0' ){ //barcode read is bad or no barcode found
    //fill red ring
    colorWipeReverse(red, 10); // Red fill the ring
    colorWipeReverse(noColor, 10); // Red fill the ring
    colorWipeReverse(red, 70); // Red fill the ring
    if(millis() - ok0Start > badBarcodeWaitTime){
      ok = '2'; //go back to loop colours  
      colorWipeReverse(red, 10); // Red fill the ring
      colorWipeReverse(noColor, 10); // Red fill the ring
      colorWipeReverse(red, 70); // Red fill the ring
      colorWipe(noColor, 10); // 
      digitalWrite(controllerRelay, LOW); //turn off the xbox controller
      gameReset = '0';
    }
  }
}//loop end

void disposeCan(void){
  Serial.println(F("disposing can"));
  Sweep.attach(sweep);
  Gate.attach(gate);
  Sweep.write(sweepHome);
  Gate.write(gateHome);
  
  for(pos = gateHome; pos>=gateAway; pos-=1)     // goes from 180 degrees to 0 degrees 
  {      
    Gate.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  }  
  Sweep.write(sweepAway);
  delay(600);
  Sweep.write(sweepHome);
  delay(600);
  //home gate
  for(int pos = gateAway ; pos <=gateHome ; pos += 1) // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    Gate.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  }
  Sweep.detach();
  Gate.detach(); 
}

void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);
  }
}

void paintStrip(uint32_t c, uint8_t number) {
  for(uint16_t i=strip.numPixels(); i>number; i--) {
      strip.setPixelColor(i, c);
      strip.show();
  }
  for(uint16_t i=number; i>0; i--) {
    strip.setPixelColor(i,strip.Color(0,0,0));
    strip.show();    
  }
}

void colorWipeReverse(uint32_t c, uint8_t wait) {
  for(uint16_t i=strip.numPixels(); i>0; i--) {
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);
  }
}

void resetGame(void){
  //tmp code
  colorWipeReverse(green, 100); //  fill the ring
}

void Init(void){
  Serial.begin(9600);
  Serial.println(F("Setting IO"));
  pinMode(barcodeButton,OUTPUT);
  pinMode(controllerRelay,OUTPUT);
  pinMode(timerButton, INPUT);

  Serial.println(F("OUTPUTS LOW"));
  digitalWrite(barcodeButton,LOW);
  digitalWrite(controllerRelay,LOW); //xbox off
  digitalWrite(timerButton, HIGH);// enable internal pull up  
  
  Serial.println(F("Attached servos"));
  
  Serial.println(F("Init LED ring"));
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'

  Serial.println(F("Set servos to home"));
  // Can.attach(canMotor);
  // Sweep.attach(sweep);
  // Gate.attach(gate);
  // Can.write(canStop); 
  // delay(200);
  // Sweep.write(sweepHome);
  // delay(200);
  // Gate.write(gateHome);
  // delay(200);
  // Can.detach();
  // Sweep.detach();
  // Gate.detach(); 
  
  barcode.begin(9600);
  Serial.flush(); //clears the buffer
  barcode.flush();//clears the buffer
  
  //empty averaging array
  for (int thisReading = 0; thisReading < numReadings; thisReading++)
    readings[thisReading] = 0;   
  Serial.println(F("All systems GO!"));
  disposeCan(); //TODO uncomment this
  Serial.println(F("Going in loop"));
}

void blinks(int frequency){

  currentMillis = millis();

    if(currentMillis - previousMillis >= frequency) {
       previousMillis = currentMillis;   

         if (ledState == LOW)
           ledState = HIGH;
       else
         ledState = LOW;
        digitalWrite(timer_led, ledState);
  }
}