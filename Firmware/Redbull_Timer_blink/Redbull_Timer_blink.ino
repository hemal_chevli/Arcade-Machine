//Redbull_Timer_blink.ino

//get data from main system
//blink accordingly

const int timer_led = 9;
const int ready_led = 10;

char inchar;
int freq = 0 ;
int lowfreq = 500;
int highfreq = 250;

int ledState = LOW;             // ledState used to set the LED
unsigned long previousMillis = 0;        // will store last time LED was updated
long interval = 1000;           // interval at which to blink (milliseconds)
unsigned long currentMillis;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(timer_led, OUTPUT);
  pinMode(ready_led, OUTPUT);
  digitalWrite(ready_led, HIGH);
}

void loop() {
	if(Serial.available()){
		inchar = Serial.read();
		Serial.print(inchar);
	//	delay(10);
	if(inchar == '1'){
			//slow
			freq = 1;
		}
		else if (inchar == '2'){
			//fast
			freq =2;
		}
		else if (inchar == '3'){
			//solid
			freq = 3;
		}
		else if (inchar == '0'){
			//off
			freq =0;
		}
	}

	if(freq == 0){
		digitalWrite(timer_led, LOW);
	}
	else if(freq == 1){
		blinks(lowfreq);
		
	}
	else if(freq == 2){
		blinks(highfreq);
	}
	else if(freq == 3){
		digitalWrite(timer_led, HIGH);
		
	}

	

}

void blinks(int frequency){

	currentMillis = millis();

		if(currentMillis - previousMillis >= frequency) {
   		 previousMillis = currentMillis;   

   			 if (ledState == LOW)
   				 ledState = HIGH;
 			 else
 			   ledState = LOW;
    		digitalWrite(timer_led, ledState);
	}
}