//redbull.ino
//arduino 1.0.6
/*
2 barcode button
3 can servo CR 100
4 relay
5 sweep 90162602servo 20-170
6 gate servo 70-150
7,8 soft serial 8,7 (rx,tx) tx to anotehr arduino
9 rgb led
10 A
11 B
12 Start
13 back
A0 up
A1 down
A2 left
A3 right
A4 timer button
A5 sharp sensor
*/

#include <Servo.h>
#include <SoftwareSerial.h>
#include <Adafruit_NeoPixel.h>

const int barcodeButton = 2;
const int canMotor = 3;
const int controllerRelay = 4;
const int sweep = 5;
const int gate = 6;
const int tx = 7;
const int rx = 8;
const int rgb = 9;
const int A = 10;
const int B = 11;
const int start = 12;
const int back = 13;
const int up = A0;
const int down = A1;
const int left = A2;
const int right = A3;
const int timerButton = A4;
const int ldr = A5;

int canStop = 90;
int canRotate = 105;
int sweepHome = 170;
int sweepAway = 20;
int gateHome = 180;
int gateAway = 70;

int sensorValue = 0;        // value read from the sensor
int i;
int pos=0;

unsigned long canDetectStart = 0;
unsigned int canScanTime = 7*1000;

unsigned int badBarcodeWaitTime = 7*1000;
unsigned long  ok0Start = 0;
unsigned long  ok1Start = 0;
unsigned long  elaspsedGameTime = 0;
unsigned long  gameTime = 240000;//2,40,000 ms
unsigned long elaspsedTimePercent = 0;

char inchar;
char codeRead[13]={};
//small can: 90162602  
//small sugarfree: 90162800
//big can : 9002490210939
char rb_small[] = {'9','0','1','6','2','6','0','2','0','0','0','0','0'};
char rb_sf[] =    {'9','0','1','6','2','8','0','0'};
//char rb_sf[]={'9','0','1','6','2','8','0','0','0','0','0','0','0'};
char rb_big[] =   {'9','0','0','2','4','9','0','2','1','0','9','3','9'};

char rb_red[] =   {'9','0','4','1','5','1','2','8','0','0','0','0','0'};
char rb_white[] = {'9','0','4','1','5','1','4','2','0','0','0','0','0'};
char rb_blue[] =  {'9','0','4','1','5','1','3','5','0','0','0','0','0'};

char ok='2';//go direct to loop and wait for can drop

char ok1='0';
char ok2='0';
char ok3='0';
char ok4='0';
char ok5='0';
char ok6='0';

char gameReset='0';
char canDetected = '0';

Servo Can;
Servo Sweep;
Servo Gate;

SoftwareSerial barcode(rx,tx); //rx.tx
//Tx connected to another arduino send command for timer led

Adafruit_NeoPixel strip = Adafruit_NeoPixel(9, rgb, NEO_GRB + NEO_KHZ800);

uint32_t red = strip.Color(127, 0, 0);
uint32_t green = strip.Color(0, 127, 0);
uint32_t blue = strip.Color(0, 0, 127);
uint32_t yellow = strip.Color(255, 100, 0);
uint32_t noColor = strip.Color(0, 0, 0);

//Sensor averaging variables
const int numReadings = 3;

int readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
int total = 0;                  // the running total
int average = 0;                // the average

void setup() {
  Init();
}
//turn on button
    //start motor
    //timer of 7 seconds
    //dispose can 
void loop() {
  delay(100);
  //if timer is up set ok at two
  total= total - readings[readIndex];         
  readings[readIndex] = analogRead(ldr); 
  total= total + readings[readIndex];       
  readIndex = readIndex + 1;                    
  if (readIndex >= numReadings){
    readIndex = 0;              
  }
  average = total / numReadings;         
  //Serial.println(average);
  if (average > 590 &&  canDetected != '1'){//Can detected
    //start timer here
    Serial.println("Can detected,timer started");
    Can.attach(canMotor);
    Can.write(canRotate); // start can rotation
    digitalWrite(barcodeButton, LOW);
    delay(100);
    digitalWrite(barcodeButton,HIGH); //press the barcode button
    canDetected = '1';
    canDetectStart = millis(); //timer start
    Serial.print("Detect start: ");
    Serial.println(canDetectStart);
  }
  else{
    //Serial.println("no Can found");
    //canDetected = '0'; //might have to remove this
  }
  if(canDetected == '1' && (millis() - canDetectStart > canScanTime)){
      Serial.println("scanning time up");
      Serial.print("Curr Time: ");
      Serial.println(millis());
      digitalWrite(barcodeButton, LOW);
      Can.write(canStop);
      Can.detach();
      disposeCan();
      canDetected = '0';
  }
  if (barcode.available() > 0){// ok variable set   
   scanAndCompare();
  }//if barcode available end

  /*
  ok = 0 -> barcode read is bad or not found
  ok = 1 -> barcode read is of redbull
  ok = 2 -> tristate variable looping
  */
  if(ok == '1'){ //barcode ok    
    elaspsedGameTime = millis() - ok1Start;
    elaspsedTimePercent = map(elaspsedGameTime,0,gameTime,0,100);
    Serial.print(elaspsedGameTime/1000);
    Serial.print("  ");
    Serial.println(elaspsedTimePercent);
    //elaspsedTimePercent = map(0,elaspsedGameTime,gameTime,0,100);
    if(gameReset == '0'){
      Can.write(canStop);
      digitalWrite(barcodeButton, LOW);
      digitalWrite(controllerRelay, HIGH);
      //t pulse here////////////////////////////////////////
      resetGame();
      //disposeCan();
      gameReset = '1';
    }
    if (elaspsedGameTime < gameTime){
      if (digitalRead(timerButton) == LOW)//monitoring the button
        {  // switch is pressed - pullup keeps pin high normally
          delay(100);                        // delay to debounce switch
          ok1Start = millis();
          //barcode.print('0');
          colorWipeReverse(green, 100); //  fill the ring
        }
      //Serial.println(elaspsedTimePercent);
      if(elaspsedTimePercent < 10){
        paintStrip(green,0);
        barcode.print('0');
      }
      if(elaspsedTimePercent >10 && elaspsedTimePercent < 20){
        //paintStrip(green,1);
      }
      if(elaspsedTimePercent >20 && elaspsedTimePercent < 30){        
        //paintStrip(green,2);
      }
      if(elaspsedTimePercent >30 && elaspsedTimePercent < 40){        
        paintStrip(green,1);
      }
      if(elaspsedTimePercent >40 && elaspsedTimePercent < 50){        
        paintStrip(green,2);
      }
      if(elaspsedTimePercent >50 && elaspsedTimePercent < 60){        
        paintStrip(green,3);
      }
      if(elaspsedTimePercent >60 && elaspsedTimePercent < 70){        
        paintStrip(green,4);
      }
      if(elaspsedTimePercent >70 && elaspsedTimePercent < 80){        
        paintStrip(yellow,5);
        barcode.print('1');//slow blink
      }
      if(elaspsedTimePercent >80 && elaspsedTimePercent < 90){        
        paintStrip(yellow,6);
        barcode.print('2'); //fast blink
      }
      if(elaspsedTimePercent >90){        
        paintStrip(red,7);
        barcode.print('3');//solid
      }
    }
    else{ //times up 
      barcode.print('0');
      digitalWrite(controllerRelay, LOW);
      gameReset = '0';
      ok = '2';
    }
  }

  else if (ok == '2'){ //loop here
    colorWipeReverse(red, 30); // Red fill the ring
    colorWipeReverse(blue, 30); // Fill the ring
     barcode.println('0');
  }

  else if (ok == '0' ){ //barcode read is bad or no barcode found
    //fill red ring
    colorWipeReverse(red, 10); // Red fill the ring
    colorWipeReverse(noColor, 10); // Red fill the ring
    colorWipeReverse(red, 70); // Red fill the ring
    if(millis() - ok0Start > badBarcodeWaitTime){
      ok = '2'; //go back to loop colours  
      //disposeCan();
      colorWipeReverse(red, 10); // Red fill the ring
      colorWipeReverse(noColor, 10); // Red fill the ring
      colorWipeReverse(red, 70); // Red fill the ring
      colorWipe(noColor, 10); // 
      digitalWrite(controllerRelay, LOW); //turn off the xbox controller
      gameReset = '0';
    }
  }
}//loop end
  
//Function prototypes
/*void detectCan(void){
  //Average values
  total= total - readings[readIndex];         
  readings[readIndex] = analogRead(ldr); 
  total= total + readings[readIndex];       
  readIndex = readIndex + 1;                    
  if (readIndex >= numReadings){
    readIndex = 0;              
  }
  average = total / numReadings;         
  Serial.println(average);
  if (average > 700 &&  canDetected != '1'){//Can detected
    //start timer here
    Serial.println("Can detected,timer started");
    canDetectStart = millis(); //timer start
    Can.attach(canMotor);
    canDetected = '1';
    digitalWrite(barcodeButton, LOW);
    Can.write(canRotate); // start can rotation
    delay(1000);
    digitalWrite(barcodeButton,HIGH); //press the barcode button
  }
  else{
    canDetected = '0';

  }
}*/

char compare(char a1 [], char a2 [], int n){
  int i, result;
  result = '1';
  for (i=0; i<n; i++){
    if (a1[i] != a2[i]){
      result = '0';
    }
  }
  return (result);
}

void disposeCan(void){
  Serial.println("disposing can");
  Sweep.attach(sweep);
  Gate.attach(gate);
  Sweep.write(sweepHome);
  Gate.write(gateHome);
  
  for(pos = gateHome; pos>=gateAway; pos-=1)     // goes from 180 degrees to 0 degrees 
  {      
    Gate.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  }
  //move arm
  for(pos = sweepHome; pos>=sweepAway; pos-=1)     // goes from 180 degrees to 0 degrees 
  {      
  
    Sweep.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  }
  //home arm
  for(int pos = sweepAway ; pos <=sweepHome ; pos += 1) // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    Sweep.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  }
  //home gate
   for(int pos = gateAway ; pos <=gateHome ; pos += 1) // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    Gate.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  }
  
  Sweep.detach();
  Gate.detach(); 
}

void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);
  }
}
void paintStrip(uint32_t c, uint8_t number) {
  for(uint16_t i=strip.numPixels(); i>number; i--) {
      strip.setPixelColor(i, c);
      strip.show();
  }
  for(uint16_t i=number; i>0; i--) {
    strip.setPixelColor(i,strip.Color(0,0,0));
    strip.show();    
  }
}

void colorWipeReverse(uint32_t c, uint8_t wait) {
  for(uint16_t i=strip.numPixels(); i>0; i--) {
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);
  }
}

void resetGame(void){
  //tmp code
  colorWipeReverse(green, 100); //  fill the ring
}

void Init(void){
  Serial.begin(9600);
  Serial.println("Setting IO");
  pinMode(barcodeButton,OUTPUT);
  pinMode(controllerRelay,OUTPUT);
  pinMode(A, OUTPUT);
  pinMode(B, OUTPUT);
  pinMode(start, OUTPUT);
  pinMode(back, OUTPUT);
  pinMode(up, OUTPUT);
  pinMode(down, OUTPUT);
  pinMode(left, OUTPUT);
  pinMode(right, OUTPUT);
  pinMode(timerButton, INPUT);

  Serial.println("OUTPUTS LOW");
  digitalWrite(barcodeButton,LOW);
  digitalWrite(controllerRelay,LOW); //xbox off
  digitalWrite(A,LOW);
  digitalWrite(B, LOW);
  digitalWrite(start, LOW);
  digitalWrite(back, LOW);
  digitalWrite(up, LOW);
  digitalWrite(down, LOW);
  digitalWrite(left, LOW);
  digitalWrite(right, LOW);
  digitalWrite(timerButton, HIGH);// enable internal pull up  
  Can.attach(canMotor);
  Sweep.attach(sweep);
  Gate.attach(gate);
  Serial.println("Attached servos");
  
  Serial.println("Init LED ring");
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'

  Serial.println("Set servos to home");
  Can.write(canStop); 
  Sweep.write(sweepAway);
  Gate.write(gateHome);
  
  Can.detach();
  Sweep.detach();
  Gate.detach(); 
  
  for(i=0;i<13;i++){ 
        codeRead[i]='0'; //empty the barcode buffer
  }  
  barcode.begin(9600);
  Serial.flush(); //clears the buffer
  barcode.flush();//clears the buffer
  barcode.println('0');//stop the timer if blinking
  //empty averaging array
  for (int thisReading = 0; thisReading < numReadings; thisReading++)
    readings[thisReading] = 0;   
  Serial.println("All systems GO!");
  disposeCan();
  Serial.println("Going in loop");
}

void scanAndCompare(void){
    Serial.println(" ");
    for(i=0;i<13;i++){
      inchar = barcode.read();
      codeRead[i]=inchar;
      Serial.print(inchar);
    }
      //delay(10);
      //compare 
      ok1 = compare(rb_small,codeRead,8);
      ok2 = compare(rb_sf,codeRead,8);
      ok3 = compare(rb_big,codeRead,13); 
      ok4 = compare(rb_red,codeRead,8);
      ok5 = compare(rb_white,codeRead,8);
      ok6 = compare(rb_blue,codeRead,8);
      
      Serial.println(" ");
      Serial.print(ok1);
      Serial.print(ok2);
      Serial.print(ok3);
      Serial.print(ok4);
      Serial.print(ok5);
      Serial.print(ok6);
    //clean buffer
    for(i=0;i<13;i++){ 
        codeRead[i]='1'; //empty the barcode buffer
    }
    if(ok1 == '1' || ok2 == '1' || ok3 == '1' || ok4 == '1' || ok5 == '1' || ok6 == '1'){
      ok  = '1';
      ok1 = '0';
      ok2 = '0';
      ok3 = '0';
      ok4 = '0';
      ok5 = '0';
      ok6 = '0';
      ok1Start = millis();
    }
    else{
      ok = '0';
      ok0Start= millis();
    }
    if( ok == '1'){
      Serial.println("Redbull Can");
    }
    else{
      Serial.println("Not RedBull Can");
    }
    //release the barcode switch and stop the rotate motor
}
